﻿using CsvHelper;
using System.Collections.Generic;
using System.IO;

namespace CSVDataloader
{
    public class DataLoader
    {
        public List<string> LoadCSV(string path)
        {
            List<string> items = new List<string>();
            using (var reader = new StreamReader(@path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    items.Add(line);
                }
            }
            return items;
        }

        public void WriteCSV(string[] newLine,string path)
        {
            using (var writer = new StreamWriter(@path))
            {
                writer.WriteLine(newLine);
                writer.Flush();
            }
        }
    }
}
