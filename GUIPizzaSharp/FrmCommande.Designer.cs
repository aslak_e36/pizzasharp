﻿namespace GUIPizzaSharp
{
    partial class FrmCommande
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCommande));
            this.grpDonneeCommande = new System.Windows.Forms.GroupBox();
            this.lblPrix = new System.Windows.Forms.Label();
            this.lblNoCommande = new System.Windows.Forms.Label();
            this.grpTypePate = new System.Windows.Forms.GroupBox();
            this.cmbPastaType = new System.Windows.Forms.ComboBox();
            this.grpIngrediants = new System.Windows.Forms.GroupBox();
            this.cmdRetirerIngrediant = new System.Windows.Forms.Button();
            this.cmdAjouterIngrediant = new System.Windows.Forms.Button();
            this.lstIngrediantsDisponibles = new System.Windows.Forms.ListBox();
            this.lstIngrediantsChoisis = new System.Windows.Forms.ListBox();
            this.cmdAnnulerCommande = new System.Windows.Forms.Button();
            this.btnValiderCommande = new System.Windows.Forms.Button();
            this.grpDonneeCommande.SuspendLayout();
            this.grpTypePate.SuspendLayout();
            this.grpIngrediants.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDonneeCommande
            // 
            this.grpDonneeCommande.Controls.Add(this.lblPrix);
            this.grpDonneeCommande.Controls.Add(this.lblNoCommande);
            this.grpDonneeCommande.ForeColor = System.Drawing.Color.Silver;
            this.grpDonneeCommande.Location = new System.Drawing.Point(13, 12);
            this.grpDonneeCommande.Name = "grpDonneeCommande";
            this.grpDonneeCommande.Size = new System.Drawing.Size(459, 65);
            this.grpDonneeCommande.TabIndex = 0;
            this.grpDonneeCommande.TabStop = false;
            this.grpDonneeCommande.Text = "Données de la commande";
            // 
            // lblPrix
            // 
            this.lblPrix.AutoSize = true;
            this.lblPrix.Location = new System.Drawing.Point(277, 33);
            this.lblPrix.Name = "lblPrix";
            this.lblPrix.Size = new System.Drawing.Size(99, 16);
            this.lblPrix.TabIndex = 1;
            this.lblPrix.Text = "Prix de la pizza : ";
            // 
            // lblNoCommande
            // 
            this.lblNoCommande.AutoSize = true;
            this.lblNoCommande.Location = new System.Drawing.Point(7, 33);
            this.lblNoCommande.Name = "lblNoCommande";
            this.lblNoCommande.Size = new System.Drawing.Size(154, 16);
            this.lblNoCommande.TabIndex = 0;
            this.lblNoCommande.Text = "Numéro de la commande :";
            // 
            // grpTypePate
            // 
            this.grpTypePate.Controls.Add(this.cmbPastaType);
            this.grpTypePate.ForeColor = System.Drawing.Color.Silver;
            this.grpTypePate.Location = new System.Drawing.Point(13, 92);
            this.grpTypePate.Name = "grpTypePate";
            this.grpTypePate.Size = new System.Drawing.Size(459, 60);
            this.grpTypePate.TabIndex = 2;
            this.grpTypePate.TabStop = false;
            this.grpTypePate.Text = "Type de pâte";
            // 
            // cmbPastaType
            // 
            this.cmbPastaType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmbPastaType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPastaType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPastaType.ForeColor = System.Drawing.SystemColors.Control;
            this.cmbPastaType.FormattingEnabled = true;
            this.cmbPastaType.Location = new System.Drawing.Point(10, 22);
            this.cmbPastaType.Name = "cmbPastaType";
            this.cmbPastaType.Size = new System.Drawing.Size(437, 24);
            this.cmbPastaType.TabIndex = 0;
            this.cmbPastaType.SelectedIndexChanged += new System.EventHandler(this.cmbPastaType_SelectedIndexChanged);
            // 
            // grpIngrediants
            // 
            this.grpIngrediants.Controls.Add(this.cmdRetirerIngrediant);
            this.grpIngrediants.Controls.Add(this.cmdAjouterIngrediant);
            this.grpIngrediants.Controls.Add(this.lstIngrediantsDisponibles);
            this.grpIngrediants.Controls.Add(this.lstIngrediantsChoisis);
            this.grpIngrediants.ForeColor = System.Drawing.Color.Silver;
            this.grpIngrediants.Location = new System.Drawing.Point(13, 169);
            this.grpIngrediants.Name = "grpIngrediants";
            this.grpIngrediants.Size = new System.Drawing.Size(459, 289);
            this.grpIngrediants.TabIndex = 3;
            this.grpIngrediants.TabStop = false;
            this.grpIngrediants.Text = "Ingrédients";
            // 
            // cmdRetirerIngrediant
            // 
            this.cmdRetirerIngrediant.BackColor = System.Drawing.Color.OrangeRed;
            this.cmdRetirerIngrediant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRetirerIngrediant.FlatAppearance.BorderSize = 0;
            this.cmdRetirerIngrediant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRetirerIngrediant.ForeColor = System.Drawing.Color.White;
            this.cmdRetirerIngrediant.Location = new System.Drawing.Point(187, 154);
            this.cmdRetirerIngrediant.Name = "cmdRetirerIngrediant";
            this.cmdRetirerIngrediant.Size = new System.Drawing.Size(84, 79);
            this.cmdRetirerIngrediant.TabIndex = 3;
            this.cmdRetirerIngrediant.Text = "Retirer";
            this.cmdRetirerIngrediant.UseVisualStyleBackColor = false;
            this.cmdRetirerIngrediant.Click += new System.EventHandler(this.cmdRetirerIngrediant_Click);
            // 
            // cmdAjouterIngrediant
            // 
            this.cmdAjouterIngrediant.BackColor = System.Drawing.Color.DodgerBlue;
            this.cmdAjouterIngrediant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouterIngrediant.FlatAppearance.BorderSize = 0;
            this.cmdAjouterIngrediant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouterIngrediant.ForeColor = System.Drawing.Color.White;
            this.cmdAjouterIngrediant.Location = new System.Drawing.Point(187, 53);
            this.cmdAjouterIngrediant.Name = "cmdAjouterIngrediant";
            this.cmdAjouterIngrediant.Size = new System.Drawing.Size(84, 79);
            this.cmdAjouterIngrediant.TabIndex = 2;
            this.cmdAjouterIngrediant.Text = "Ajouter";
            this.cmdAjouterIngrediant.UseVisualStyleBackColor = false;
            this.cmdAjouterIngrediant.Click += new System.EventHandler(this.cmdAjouterIngrediant_Click);
            // 
            // lstIngrediantsDisponibles
            // 
            this.lstIngrediantsDisponibles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.lstIngrediantsDisponibles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstIngrediantsDisponibles.ForeColor = System.Drawing.SystemColors.Control;
            this.lstIngrediantsDisponibles.FormattingEnabled = true;
            this.lstIngrediantsDisponibles.ItemHeight = 16;
            this.lstIngrediantsDisponibles.Location = new System.Drawing.Point(277, 23);
            this.lstIngrediantsDisponibles.Name = "lstIngrediantsDisponibles";
            this.lstIngrediantsDisponibles.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstIngrediantsDisponibles.Size = new System.Drawing.Size(170, 240);
            this.lstIngrediantsDisponibles.TabIndex = 1;
            // 
            // lstIngrediantsChoisis
            // 
            this.lstIngrediantsChoisis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.lstIngrediantsChoisis.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstIngrediantsChoisis.ForeColor = System.Drawing.SystemColors.Control;
            this.lstIngrediantsChoisis.FormattingEnabled = true;
            this.lstIngrediantsChoisis.ItemHeight = 16;
            this.lstIngrediantsChoisis.Location = new System.Drawing.Point(10, 23);
            this.lstIngrediantsChoisis.Name = "lstIngrediantsChoisis";
            this.lstIngrediantsChoisis.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstIngrediantsChoisis.Size = new System.Drawing.Size(170, 240);
            this.lstIngrediantsChoisis.TabIndex = 0;
            // 
            // cmdAnnulerCommande
            // 
            this.cmdAnnulerCommande.BackColor = System.Drawing.Color.OrangeRed;
            this.cmdAnnulerCommande.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnulerCommande.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cmdAnnulerCommande.FlatAppearance.BorderSize = 0;
            this.cmdAnnulerCommande.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnulerCommande.Font = new System.Drawing.Font("Bahnschrift SemiLight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnulerCommande.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmdAnnulerCommande.Location = new System.Drawing.Point(12, 479);
            this.cmdAnnulerCommande.Name = "cmdAnnulerCommande";
            this.cmdAnnulerCommande.Size = new System.Drawing.Size(219, 50);
            this.cmdAnnulerCommande.TabIndex = 4;
            this.cmdAnnulerCommande.Text = "Annuler";
            this.cmdAnnulerCommande.UseVisualStyleBackColor = false;
            this.cmdAnnulerCommande.Click += new System.EventHandler(this.CmdAnnulerCommande_Click);
            // 
            // btnValiderCommande
            // 
            this.btnValiderCommande.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnValiderCommande.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnValiderCommande.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnValiderCommande.FlatAppearance.BorderSize = 0;
            this.btnValiderCommande.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnValiderCommande.Font = new System.Drawing.Font("Bahnschrift SemiLight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValiderCommande.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnValiderCommande.Location = new System.Drawing.Point(253, 479);
            this.btnValiderCommande.Name = "btnValiderCommande";
            this.btnValiderCommande.Size = new System.Drawing.Size(219, 50);
            this.btnValiderCommande.TabIndex = 5;
            this.btnValiderCommande.Text = "Valider";
            this.btnValiderCommande.UseVisualStyleBackColor = false;
            this.btnValiderCommande.Click += new System.EventHandler(this.btnValiderCommande_Click);
            // 
            // FrmCommande
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(484, 541);
            this.Controls.Add(this.btnValiderCommande);
            this.Controls.Add(this.cmdAnnulerCommande);
            this.Controls.Add(this.grpIngrediants);
            this.Controls.Add(this.grpTypePate);
            this.Controls.Add(this.grpDonneeCommande);
            this.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCommande";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Concevoir la pizza";
            this.Load += new System.EventHandler(this.FrmCommande_Load);
            this.grpDonneeCommande.ResumeLayout(false);
            this.grpDonneeCommande.PerformLayout();
            this.grpTypePate.ResumeLayout(false);
            this.grpIngrediants.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDonneeCommande;
        private System.Windows.Forms.Label lblPrix;
        private System.Windows.Forms.Label lblNoCommande;
        private System.Windows.Forms.GroupBox grpTypePate;
        private System.Windows.Forms.GroupBox grpIngrediants;
        private System.Windows.Forms.ListBox lstIngrediantsDisponibles;
        private System.Windows.Forms.ListBox lstIngrediantsChoisis;
        private System.Windows.Forms.Button cmdAjouterIngrediant;
        private System.Windows.Forms.Button cmdRetirerIngrediant;
        private System.Windows.Forms.Button cmdAnnulerCommande;
        private System.Windows.Forms.Button btnValiderCommande;
        private System.Windows.Forms.ComboBox cmbPastaType;
    }
}