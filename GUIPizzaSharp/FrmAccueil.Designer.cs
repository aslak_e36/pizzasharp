﻿namespace GUIPizzaSharp
{
    partial class FrmAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAccueil));
            this.lblVosCommandes = new System.Windows.Forms.Label();
            this.lstCommandes = new System.Windows.Forms.ListBox();
            this.cmdNouvelleCommande = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblVosCommandes
            // 
            this.lblVosCommandes.AutoSize = true;
            this.lblVosCommandes.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVosCommandes.Location = new System.Drawing.Point(12, 163);
            this.lblVosCommandes.Name = "lblVosCommandes";
            this.lblVosCommandes.Size = new System.Drawing.Size(101, 16);
            this.lblVosCommandes.TabIndex = 1;
            this.lblVosCommandes.Text = "Les commandes";
            // 
            // lstCommandes
            // 
            this.lstCommandes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.lstCommandes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstCommandes.Font = new System.Drawing.Font("Bahnschrift SemiLight", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCommandes.ForeColor = System.Drawing.SystemColors.Control;
            this.lstCommandes.FormattingEnabled = true;
            this.lstCommandes.ItemHeight = 16;
            this.lstCommandes.Location = new System.Drawing.Point(12, 192);
            this.lstCommandes.Name = "lstCommandes";
            this.lstCommandes.Size = new System.Drawing.Size(330, 178);
            this.lstCommandes.TabIndex = 2;
            // 
            // cmdNouvelleCommande
            // 
            this.cmdNouvelleCommande.BackColor = System.Drawing.Color.OrangeRed;
            this.cmdNouvelleCommande.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdNouvelleCommande.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cmdNouvelleCommande.FlatAppearance.BorderSize = 0;
            this.cmdNouvelleCommande.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdNouvelleCommande.Font = new System.Drawing.Font("Bahnschrift SemiLight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdNouvelleCommande.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmdNouvelleCommande.Location = new System.Drawing.Point(12, 389);
            this.cmdNouvelleCommande.Name = "cmdNouvelleCommande";
            this.cmdNouvelleCommande.Size = new System.Drawing.Size(330, 50);
            this.cmdNouvelleCommande.TabIndex = 3;
            this.cmdNouvelleCommande.Text = "Nouvelle commande";
            this.cmdNouvelleCommande.UseVisualStyleBackColor = false;
            this.cmdNouvelleCommande.Click += new System.EventHandler(this.CmdNouvelleCommande_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(101, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // FrmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(354, 451);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cmdNouvelleCommande);
            this.Controls.Add(this.lstCommandes);
            this.Controls.Add(this.lblVosCommandes);
            this.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAccueil";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Page d\'accueil";
            this.Load += new System.EventHandler(this.FrmAccueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblVosCommandes;
        private System.Windows.Forms.ListBox lstCommandes;
        private System.Windows.Forms.Button cmdNouvelleCommande;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

