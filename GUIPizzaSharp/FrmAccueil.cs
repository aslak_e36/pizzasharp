﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CSVDataloader;

namespace GUIPizzaSharp
{
    public partial class FrmAccueil : Form
    {
        public FrmAccueil()
        {
            InitializeComponent();
        }

        private void FrmAccueil_Load(object sender, EventArgs e)
        {
            DataLoader dataLoader = new DataLoader();
            List<String> pizzas = dataLoader.LoadCSV("../../../Data/Pizzas.csv");
        }

        private void CmdNouvelleCommande_Click(object sender, EventArgs e)
        {
            FrmCommande frmCommande = new FrmCommande();
            DialogResult dialogResult = frmCommande.ShowDialog();
        }
    }
}
