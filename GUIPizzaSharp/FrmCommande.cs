﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CSVDataloader;

namespace GUIPizzaSharp
{
    public partial class FrmCommande : Form
    {
        public FrmCommande()
        {
            InitializeComponent();
        }

        private void FrmCommande_Load(object sender, EventArgs e)
        {
            DataLoader dataLoader = new DataLoader();
            List<String> pastaType = dataLoader.LoadCSV("../../../Data/PastaType.csv");
            foreach (var item in pastaType)
            {
                var value = item.Split(';');
                cmbPastaType.Items.Add(value[1]);
            }
            cmbPastaType.SelectedIndex = 0;

            List<String> components = dataLoader.LoadCSV("../../../Data/components.csv");
            foreach (var item in components)
            {
                var value = item.Split(';');
                lstIngrediantsDisponibles.Items.Add(value[1]);
            }
        }

        private void CmdAnnulerCommande_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cmbPastaType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataLoader dataLoader = new DataLoader();
            List<String> pastaType = dataLoader.LoadCSV("../../../Data/PastaType.csv");
            foreach (var item in pastaType)
            {
                var value = item.Split(';');
                int id;
                Int32.TryParse(value[0], out id);
                if (cmbPastaType.SelectedIndex == id)
                {
                    var v = lblPrix.Text.Substring(0, 18);
                    v += " " + value[2] + " CHF";
                    lblPrix.Text = v;
                }
            }
        }

        private void btnValiderCommande_Click(object sender, EventArgs e)
        {
            DataLoader dataLoader = new DataLoader();
            string[] newCommand = { "1", "Pizza Classique", "ingrédiants" };
            dataLoader.WriteCSV(newCommand, "../../../Data/Pizzas.csv");
            this.DialogResult = DialogResult.OK;
        }

        private void cmdAjouterIngrediant_Click(object sender, EventArgs e)
        {
            if (lstIngrediantsDisponibles.SelectedItem != null)
            {
                List<string> items = new List<string>();
                items = lstIngrediantsDisponibles.SelectedItems.Cast<string>().ToList();
                foreach (var item in items)
                {
                    lstIngrediantsChoisis.Items.Add(lstIngrediantsDisponibles.SelectedItem);
                    lstIngrediantsDisponibles.Items.Remove(lstIngrediantsDisponibles.SelectedItem);
                }
            }
        }

        private void cmdRetirerIngrediant_Click(object sender, EventArgs e)
        {
            if (lstIngrediantsChoisis.SelectedItem != null)
            {
                List<string> items = new List<string>();
                items = lstIngrediantsChoisis.SelectedItems.Cast<string>().ToList();
                foreach (var item in items)
                {
                    lstIngrediantsDisponibles.Items.Add(lstIngrediantsChoisis.SelectedItem);
                    lstIngrediantsChoisis.Items.Remove(lstIngrediantsChoisis.SelectedItem);
                }
            }
        }
    }
}
