﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestPizzaSharp
{
    [TestClass]
    public class TestPizzaSharp
    {
        [TestMethod]
        public void TestMethod1()
        {
            ///Contexte de départ

            ///L'application est ouverte sur l'écran d'accueil.

            ///Evénement

            ///Je choisis "nouvelle pizza"

            ///Résultat

            ///L'écran "concevoir sa pizza" s'affiche.Seul le choix de la pâte est disponible.
        }

        [TestMethod]
        public void TestMethod2()
        {
            ///Contexte de départ

            //L'écran "concevoir sa pizza" s'affiche.J'ai choisi le type de pâtes. Une liste d'ingrédients est disponible.

            ///Evénement

            //Je choisis un ou plusieurs ingrédients à ajouter sur la pizza.

            ///Résultat

            //La pizza se construit au fur et à mesure que j'ajoute les ingrédients. 
            //Je peux sauvegarder à tout moment, sauf lorsque la pizza ne contient aucun ingrédient. 
            //Les ingrédients qui sont ajoutés à la pizza disparaissent de la liste des ingrédients disponible. 
            //Le prix de la pizza s'adapte à chaque ajout/suppression d'ingrédient.
        }

        [TestMethod]
        public void TestMethod3()
        {
            ///Contexte de départ

            //L'application est ouverte sur l'écran d'accueil. 
            //Une ou plusieurs commandes est/sont disponible/s.

            ///Evénement

            //Je choisis une commande existante.

            ///Résultat

            //L'écran "concevoir sa pizza" s'affiche.
            //Le type de pâte ainsi que les ingrédients composants la pizza s'affichent.
        }

        [TestMethod]
        public void TestMethod4()
        {
            ///Contexte de départ

            //L'écran "concevoir sa pizza" est affiché. Le type de pâte ainsi que les ingrédients composants la pizza sont affichés.

            ///Evénement

            //J'actionne le bouton "modifier la pizza".

            ///Résultat

            //Il est désormais possible de modifier la pâte ainsi que les ingrédients, puis d'enregistrer la nouvelle pizza. 
            //La pizza garde le même numéro de commande.
        }

        [TestMethod]
        public void TestMethod5()
        {
            ///Contexte de départ

            //L'application est arrêtée.

            ///Evénement

            //Je lance l'application.

            ///Résultat

            //Les éléments suivants sont chargés dans l'application depuis des fichiers plats: 
            //les pizzas déjà commandées les types de pâtes disponibles
            //les types d'ingrédients disponibles
        }
    }
}
